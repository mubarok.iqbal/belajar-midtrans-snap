<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <div id="app-5">
      <button v-on:click="handlePayButton">Pay</button>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="https://app.sandbox.midtrans.com/snap/snap.js" data-client-key="SB-Mid-client-8W72mg5NSaafIsA2"></script>
    <script type="text/javascript">
      var app5 = new Vue({
      el: '#app-5',

      methods: {
        handlePayButton: function () {
          let requestBody = {
            transaction_details: {
              gross_amount: 123000,
              // as example we use timestamp as order ID
              order_id: 'T-'+Math.round((new Date()).getTime() / 1000)
            },
            credit_card: {
              secure: true
            }
          }

          this.getSnapToken(requestBody, function(response){
            console.log(response)
            var response = JSON.parse(response);
            snap.pay(response.token);
          });

        },
        getSnapToken: function(requestBody , callback){
          var xmlHttp = new XMLHttpRequest();
          xmlHttp.onreadystatechange = function() {
            if(xmlHttp.readyState == 4 && xmlHttp.status == 200) {
              callback(xmlHttp.responseText);
            }
          }
          xmlHttp.open("post", "/api/midtrans");
          xmlHttp.send(JSON.stringify(requestBody));
        }

      }
      })
    </script>
  </body>

</html>

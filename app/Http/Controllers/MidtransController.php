<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class MidtransController extends Controller
{
    public function storeTransaction(Request $request){

      $server_key = config('app.midtrans.server_key');
      $base_uri = config('app.midtrans.base_uri');
      $client = new \GuzzleHttp\Client();
      $client->request('POST', '/get', [
          'headers' => [
              'Accept'            =>  'application/json',
              'Authorization'     =>  'Basic ' . $server_key,
              'Content-Type'      =>  'application/json'
          ],
          'form_params' => [
              'payment_type' => 'bank_transfer',
              'transaction_details' => [
                  "order_id": "order-101",
                  "gross_amount": 44000
              ],
              'bank_transfer' => [
                  'bank': 'bca'
              ]
          ]

      ]);

      dd(json_decode($api_request->getBody());
      );
    }

    public function testTransaction(){
      
    }
}

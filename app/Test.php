<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use App\Traits\UsesUuid;

class Test extends Model
{

  use UsesUuid;

  protected $guarded = [];
}
